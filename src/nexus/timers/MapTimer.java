package net.theminegames.nexus.timers;

import net.theminegames.nexus.Nexus;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class MapTimer implements Runnable {

	private static int timer = 90;
	private static int taskID;
	private static boolean running = false;

	private static Nexus instance;

	public MapTimer(Nexus instance) {
		MapTimer.instance = instance;
	}

	@Override
	public void run() {
		timer--;

		instance.obj.setDisplayName(ChatColor.GREEN + "" + ChatColor.STRIKETHROUGH + "--" + ChatColor.RESET + " " + ChatColor.GREEN + Nexus.millisToReadable(timer) + " " + ChatColor.STRIKETHROUGH + "--");
			for(Player p : Bukkit.getOnlinePlayers()) {
				instance.newScoreboard(p);
			}
		
		if (!ready()) {
			stop();
			resetTimer();
			Bukkit.broadcastMessage(ChatColor.GREEN + "[Nexus] " + ChatColor.GOLD + "Not enough players, timer stopping.");
		}

		switch (timer) {
		case 20:
		case 10:
		case 5:
		case 4:
		case 3:
		case 2:
		case 1:
			Bukkit.broadcastMessage(ChatColor.GREEN + "[Nexus] " + ChatColor.GOLD + "Next map is " + ChatColor.RED + instance.getMaps().getMapName() + ChatColor.GOLD + " starting in " + timer
					+ " seconds");
			break;
		}

		if (timer == 0) {
			stop();

			for (Player p : Bukkit.getOnlinePlayers()) {
				instance.getGame().initPlayerToTeams(p);
				instance.getGame().initScoreboard(p);
			}

			for (OfflinePlayer pl : instance.getGame().getTeamRed().getPlayers()) {
				pl.getPlayer().teleport(instance.getMaps().getRedTeamSpawn());
				instance.getGame().givePlayerItems(pl.getPlayer());
			}

			for (OfflinePlayer pl : instance.getGame().getTeamBlue().getPlayers()) {
				pl.getPlayer().teleport(instance.getMaps().getBlueTeamSpawn());
				instance.getGame().givePlayerItems(pl.getPlayer());
			}
		}
	}

	public static int getTaskId() {
		return taskID;
	}

	public static void resetTimer() {
		timer = 90;
		instance.obj.setDisplayName(ChatColor.GREEN + "" + ChatColor.STRIKETHROUGH + "--" + ChatColor.RESET + " " + ChatColor.GREEN + Nexus.millisToReadable(timer) + " " + ChatColor.STRIKETHROUGH
				+ "--");
	}

	public static boolean ready() {
		return Bukkit.getOnlinePlayers().length >= 2;
	}

	public static int getTimer() {
		return timer;
	}

	public static void start() {
		running = true;
		taskID = schedule();
	}

	public static void stop() {
		running = false;
		Bukkit.getScheduler().cancelTask(taskID);
	}

	private static int schedule() {
		return Bukkit.getScheduler().runTaskTimer(instance, new MapTimer(instance), 0L, 20L).getTaskId();
	}

	public static boolean isFinished() {
		if(timer <= 0) {
			return true;
		}
		return false;
	}

	public static boolean isRunning() {
		return running;
	}

	public static void setTimer(int i) {
		timer = i;
	}
}
