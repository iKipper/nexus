package net.theminegames.nexus.commands;

import net.theminegames.nexus.timers.MapTimer;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class GameCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if(s.isOp()) {
			if(args.length < 1) {
				s.sendMessage(ChatColor.RED + "/game <quickstart>");
			} else if(args[0].equals("quickstart")) {
				MapTimer.setTimer(6);
			}
		} else {
			s.sendMessage(ChatColor.RED + "No permission.");
		}
		return false;
	}

}
