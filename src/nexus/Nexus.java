package net.theminegames.nexus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import me.tku.Database.Database;
import net.theminegames.nexus.apis.Points;
import net.theminegames.nexus.apis.ServerApi;
import net.theminegames.nexus.commands.GameCommand;
import net.theminegames.nexus.game.Game;
import net.theminegames.nexus.game.Map;
import net.theminegames.nexus.game.Rollback;
import net.theminegames.nexus.timers.MapTimer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

public class Nexus extends JavaPlugin {

	private static Nexus instance;
	private Points points;
	private Map maps;
	private Game game;
	private Rollback rollback;
	private MapTimer mapTimer;
	public Objective obj;

	public void onEnable() {
		saveDefaultConfig();

		instance = this;
		points = new Points();
		maps = new Map(this);
		game = new Game(this);
		mapTimer = new MapTimer(this);

		for (Player p : Bukkit.getOnlinePlayers()) {
			ServerApi.sendPlayer(p, "hub");
		}

		Database.connectMysql();

		getCommand("game").setExecutor(new GameCommand());

		Bukkit.getPluginManager().registerEvents(new NexusListener(this), this);
		Bukkit.getPluginManager().registerEvents(new NexusMenu(this), this);

		databaseReconnector();

		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "gamerule doDaylightCycle false");

		for(String s : maps.maps) {
			try {
				delete(new File(s));
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			Rollback.rollback(s);
		}

		instance.getMaps().mapRotation();
	}

	public void onDisable() {
		Database.closeConnection();
	}

	public void delete(File f) throws IOException {
		if (f.isDirectory()) {
			for (File c : f.listFiles())
				delete(c);
		}
		if (!f.delete())
			throw new FileNotFoundException("Failed to delete file: " + f);
	}

	public Map getMaps() {
		return maps;
	}

	public Game getGame() {
		return game;
	}

	public Rollback getRollback() {
		return rollback;
	}

	public static Nexus getInstance() {
		return instance;
	}

	public Points getPoints() {
		return points;
	}

	public MapTimer getMapTimer() {
		return mapTimer;
	}

	public void unloadMap(String mapname) {
		if (Bukkit.getServer().unloadWorld(Bukkit.getServer().getWorld(mapname), false)) {
			System.out.println("Map unloaded: " + mapname);
		} else {
			System.out.println("Map couldn't unload");
		}
	}

	public void loadMap(String mapname) {
		World w = Bukkit.getServer().createWorld(new WorldCreator(mapname));
		w.setAutoSave(false);
		w.setWeatherDuration(0);
		w.setTime(0);
		w.setThundering(false);
		System.out.println(w.getName() + " loaded");
	}

	public static String millisToReadable(long millis) {
		int x = (int) (millis);
		int seconds = x % 60;
		x /= 60;
		int minutes = x % 60;
		x /= 60;
		if (seconds < 10)
			return "0" + minutes + ":0" + seconds;
		else
			return "0" + minutes + ":" + seconds;
	}

	public void removeEntities() {
		for (World world : Bukkit.getWorlds()) {
			for (Entity en : world.getEntities()) {
				if (en.getType() != EntityType.PLAYER && en.getType() != EntityType.ITEM_FRAME) {
					en.remove();
				}
			}
		}
	}

	public void databaseReconnector() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				Database.closeConnection();
				Database.connectMysql();
			}
		}, 0, 20 * 60 * 5);
	}

	public void newScoreboard(Player p) {
		Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		obj = board.registerNewObjective("mmm", "mmm");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);

		Score max = obj.getScore(Bukkit.getOfflinePlayer(ChatColor.WHITE + "" + ChatColor.BOLD + "Min Players"));
		max.setScore(8);

		Score maxNum = obj.getScore(Bukkit.getOfflinePlayer(ChatColor.GRAY + "" + ChatColor.UNDERLINE + "2"));
		maxNum.setScore(7);

		Score min = obj.getScore(Bukkit.getOfflinePlayer(ChatColor.WHITE + "" + ChatColor.BOLD + "Players"));
		min.setScore(5);

		Score minNum = obj.getScore(Bukkit.getOfflinePlayer(ChatColor.GRAY + "" + ChatColor.UNDERLINE + Bukkit.getOnlinePlayers().length));
		minNum.setScore(4);

		Score empty = obj.getScore(Bukkit.getOfflinePlayer(" "));
		empty.setScore(6);

		Score empty1 = obj.getScore(Bukkit.getOfflinePlayer("  "));
		empty1.setScore(3);

		Score map = obj.getScore(Bukkit.getOfflinePlayer(ChatColor.WHITE + "" + ChatColor.BOLD + "Next Map"));
		map.setScore(2);

		Score mapName = obj.getScore(Bukkit.getOfflinePlayer(ChatColor.GRAY + "" + ChatColor.UNDERLINE + maps.getMapName()));
		mapName.setScore(1);

		obj.setDisplayName(ChatColor.GREEN + "" + ChatColor.STRIKETHROUGH + "--" + ChatColor.RESET + " " + ChatColor.GREEN + millisToReadable(MapTimer.getTimer()) + " " + ChatColor.STRIKETHROUGH
				+ "--");
		p.setScoreboard(board);
	}
}