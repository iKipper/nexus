package net.theminegames.nexus;

import net.theminegames.nexus.apis.IconMenu;
import net.theminegames.nexus.apis.IconMenu.OptionClickEvent;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class NexusMenu implements Listener {

	private Nexus instance;
	private IconMenu shop;
	private Inventory inv;

	public NexusMenu(Nexus instance) {
		this.instance = instance;
	}

	@EventHandler
	public void onPlayerInteract(final PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (e.getPlayer().getItemInHand().equals(getMenuIcon())) {
				inv = Bukkit.createInventory(null, 9, "Nexus Menu");
				addMenuItems();
				e.getPlayer().openInventory(inv);
			}
		}
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		if (e.getInventory().getName().equalsIgnoreCase(ChatColor.GOLD + "Nexus Shop")) {
			shop.destroy();
		}
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if(e.getInventory().getName().equalsIgnoreCase("Nexus Menu")) {
			e.setCancelled(true);
			final Player p = (Player) e.getWhoClicked();
			
			if(e.getCurrentItem().equals(getShopIcon())) {
				
				final ItemStack item = new ItemStack(Material.STICK);
				item.addUnsafeEnchantment(Enchantment.KNOCKBACK, 2);
				final ItemStack dSword = new ItemStack(Material.DIAMOND_SWORD);
				dSword.addEnchantment(Enchantment.KNOCKBACK, 1);
				final ItemStack gApple = new ItemStack(Material.GOLDEN_APPLE, 3);
				final ItemStack[] iArmour = { new ItemStack(Material.IRON_BOOTS), new ItemStack(Material.IRON_LEGGINGS), new ItemStack(Material.IRON_CHESTPLATE), new ItemStack(Material.IRON_HELMET) };
				final ItemStack[] dArmour = { new ItemStack(Material.DIAMOND_BOOTS), new ItemStack(Material.DIAMOND_LEGGINGS), new ItemStack(Material.DIAMOND_CHESTPLATE),
						new ItemStack(Material.DIAMOND_HELMET) };

				shop = new IconMenu(ChatColor.GOLD + "Nexus Shop", 36, new IconMenu.OptionClickEventHandler() {
					@SuppressWarnings("deprecation")
					@Override
					public void onOptionClick(OptionClickEvent event) {
						if (event.getPosition() == 0) {
							if (instance.getPoints().getPlayerPoints().get(p.getName()) >= 3) {
								event.getPlayer().getInventory().addItem(item);
								instance.getPoints().getPlayerPoints().put(p.getName(), instance.getPoints().getPlayerPoints().get(p.getName()) - 3);
							} else {
								event.getPlayer().sendMessage(ChatColor.RED + "You do not have enough points.");
							}
						} else if (event.getPosition() == 1) {
							if (instance.getPoints().getPlayerPoints().get(p.getName()) >= 3) {
								event.getPlayer().getInventory().addItem(gApple);
								instance.getPoints().getPlayerPoints().put(p.getName(), instance.getPoints().getPlayerPoints().get(p.getName()) - 3);
							} else {
								event.getPlayer().sendMessage(ChatColor.RED + "You do not have enough points.");
							}
						} else if (event.getPosition() == 2) {
							if (instance.getPoints().getPlayerPoints().get(p.getName()) >= 5) {
								event.getPlayer().getInventory().addItem(dSword);
								instance.getPoints().getPlayerPoints().put(p.getName(), instance.getPoints().getPlayerPoints().get(p.getName()) - 5);
							} else {
								event.getPlayer().sendMessage(ChatColor.RED + "You do not have enough points.");
							}
						} else if (event.getPosition() == 3) {
							if (instance.getPoints().getPlayerPoints().get(p.getName()) >= 7) {
								event.getPlayer().getInventory().setArmorContents(iArmour);
								instance.getPoints().getPlayerPoints().put(p.getName(), instance.getPoints().getPlayerPoints().get(p.getName()) - 7);
							} else {
								event.getPlayer().sendMessage(ChatColor.RED + "You do not have enough points.");
							}
						} else if (event.getPosition() == 4) {
							if (instance.getPoints().getPlayerPoints().get(p.getName()) >= 10) {
								event.getPlayer().getInventory().setArmorContents(dArmour);
								instance.getPoints().getPlayerPoints().put(p.getName(), instance.getPoints().getPlayerPoints().get(p.getName()) - 10);
							} else {
								event.getPlayer().sendMessage(ChatColor.RED + "You do not have enough points.");
							}
						} else if (event.getPosition() == 5) {
							if (instance.getPoints().getPlayerPoints().get(p.getName()) >= 20) {
								event.getPlayer().getInventory().addItem(new ItemStack(Material.TNT));
								instance.getPoints().getPlayerPoints().put(p.getName(), instance.getPoints().getPlayerPoints().get(p.getName()) - 20);
							} else {
								event.getPlayer().sendMessage(ChatColor.RED + "You do not have enough points.");
							}
						} else if (event.getPosition() == 9) {
							if (instance.getPoints().getPlayerPoints().get(p.getName()) >= 4) {
								event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 60 * 5, 0));
								instance.getPoints().getPlayerPoints().put(p.getName(), instance.getPoints().getPlayerPoints().get(p.getName()) - 4);
							} else {
								event.getPlayer().sendMessage(ChatColor.RED + "You do not have enough points.");
							}
						} else if (event.getPosition() == 10) {
							if (instance.getPoints().getPlayerPoints().get(p.getName()) >= 7) {
								event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 60 * 5, 0));
								instance.getPoints().getPlayerPoints().put(p.getName(), instance.getPoints().getPlayerPoints().get(p.getName()) - 7);
							} else {
								event.getPlayer().sendMessage(ChatColor.RED + "You do not have enough points.");
							}
						}

						event.setWillClose(true);
						event.setWillDestroy(true);
						event.getPlayer().updateInventory();
					}
				}, instance)
						.setOption(0, item, ChatColor.GREEN + "The Player Tosser", ChatColor.GOLD + "" + ChatColor.ITALIC + "Costs: 3")
						.setOption(1, gApple, ChatColor.GREEN + "Golden Apple", ChatColor.GOLD + "" + ChatColor.ITALIC + "Costs: 3")
						.setOption(2, dSword, ChatColor.GREEN + "Diamond Sword", ChatColor.GOLD + "" + ChatColor.ITALIC + "Costs: 5")
						.setOption(3, new ItemStack(Material.IRON_BLOCK), ChatColor.GREEN + "Iron Armour", ChatColor.GOLD + "" + ChatColor.ITALIC + "Costs: 7")
						.setOption(4, new ItemStack(Material.DIAMOND_BLOCK), ChatColor.GREEN + "Diamond Armour", ChatColor.GOLD + "" + ChatColor.ITALIC + "Costs: 10")
						.setOption(5, new ItemStack(Material.TNT), ChatColor.GREEN + "Exploding TnT", ChatColor.GOLD + "" + ChatColor.ITALIC + "Costs: 20")
						.setOption(9, new ItemStack(Material.POTION, 1, (short) 2), ChatColor.GREEN + "Speed Potion I", ChatColor.GOLD + "" + ChatColor.ITALIC + "Costs: 4")
						.setOption(10, new ItemStack(Material.POTION, 1, (short) 9), ChatColor.GREEN + "Strength Potion I", ChatColor.GOLD + "" + ChatColor.ITALIC + "Costs: 7")
						.setOption(31, new ItemStack(Material.DIAMOND), ChatColor.GOLD + "Your points: " + instance.getPoints().getPlayerPoints().get(p.getName()),
								ChatColor.WHITE + "" + ChatColor.ITALIC + "Your current points");
				shop.open(p);
			}
			
			if(e.getCurrentItem().equals(getKitIcon())) {
				p.closeInventory();
				p.sendMessage(ChatColor.AQUA + "Coming soon...");
			}
		}
	}

	private void addMenuItems() {
		inv.clear();
		inv.setItem(2, getShopIcon());
		inv.setItem(6, getKitIcon());
	}
	
	private ItemStack getShopIcon() {
		ItemStack is = new ItemStack(Material.EMERALD);
		is.addUnsafeEnchantment(Enchantment.DURABILITY, 1);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Nexus Shop");
		is.setItemMeta(im);
		return is;
	}
	
	private ItemStack getKitIcon() {
		ItemStack is = new ItemStack(Material.DIAMOND_SWORD);
		is.addUnsafeEnchantment(Enchantment.DURABILITY, 1);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Nexus Kits");
		is.setItemMeta(im);
		return is;
	}
	
	public static ItemStack getMenuIcon() {
		ItemStack is = new ItemStack(Material.NETHER_STAR);
		is.addUnsafeEnchantment(Enchantment.DURABILITY, 1);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Nexus Menu");
		is.setItemMeta(im);
		return is;
	}
}
