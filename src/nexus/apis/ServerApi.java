package net.theminegames.nexus.apis;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.theminegames.nexus.Nexus;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ServerApi {
	public static void sendPlayer(Player p, String server) {
		Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(Nexus.getInstance(), "BungeeCord");
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(b);
		try {
			out.writeUTF("Connect");
			out.writeUTF(server);
		} catch(IOException e) {
			e.printStackTrace();
		}
		p.sendPluginMessage(Nexus.getInstance(), "BungeeCord", b.toByteArray());
	}
}
