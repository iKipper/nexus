package net.theminegames.nexus.apis;

import java.util.HashMap;


public class Points {
	private HashMap<String, Integer> points;
	
	public Points() {
		points = new HashMap<String, Integer>();
	}
	
	public HashMap<String, Integer> getPlayerPoints() {
		return points;
	}
}