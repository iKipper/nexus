package net.theminegames.nexus.apis;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import me.tku.Database.Database;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class DatabaseApi {
	
	public static String getPlayerRank(Player p) {
		try {
			String toRun = "SELECT Rank FROM TheMineGamesPlayers WHERE PlayerName = ?";
			PreparedStatement ps = Database.getConnection().prepareStatement(toRun);
			ps.setString(1, p.getName());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static int getPlayerTokens(Player p) {
		try {
			String toRun = "SELECT Tokens FROM TheMineGamesPlayers WHERE PlayerName = ?";
			PreparedStatement ps = Database.getConnection().prepareStatement(toRun);
			ps.setString(1, p.getName());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public static void addPlayerTokens(Player p, int amount) {
		try {
			if(checkPlayerVIP(p)) {
				String toRun = "UPDATE TheMineGamesPlayers SET Tokens = ? WHERE PlayerName = ?";
				PreparedStatement ps = Database.getConnection().prepareStatement(toRun);
				ps.setInt(1, getPlayerTokens(p) + (amount * 2));
				ps.setString(2, p.getName());
				ps.executeUpdate();
				p.sendMessage(ChatColor.GOLD + "" + ChatColor.ITALIC + "+" + (amount * 2) + " Token(s) " + ChatColor.YELLOW + "(VIP x2 Tokens)");
			} else {
				String toRun = "UPDATE TheMineGamesPlayers SET Tokens = ? WHERE PlayerName = ?";
				PreparedStatement ps = Database.getConnection().prepareStatement(toRun);
				ps.setInt(1, getPlayerTokens(p) + amount);
				ps.setString(2, p.getName());
				ps.executeUpdate();
				p.sendMessage(ChatColor.GOLD + "" + ChatColor.ITALIC + "+" + amount + " Token(s)");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean checkPlayerVIP(Player p) {
		try {
			String toRun = "SELECT Rank FROM TheMineGamesPlayers WHERE PlayerName = ?";
			PreparedStatement ps = Database.getConnection().prepareStatement(toRun);
			ps.setString(1, p.getName());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				if(rs.getString("Rank").equalsIgnoreCase("VIP"))
					return true;
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
