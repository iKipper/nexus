package net.theminegames.nexus.game;

import java.util.List;
import java.util.Random;

import net.theminegames.nexus.Nexus;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Map {

	public String mapName;
	private int mapId = 0;
	private Nexus instance;
	private boolean randomMap = false;

	public List<String> maps;

	public Map(Nexus instance) {
		this.instance = instance;
		maps = instance.getConfig().getStringList("Maps");
	}

	public String getMapName() {
		return mapName;
	}

	public boolean mapRotationFinished() {
		if (mapId == maps.size()) {
			return true;
		}
		return false;
	}

	public void mapRotation() {
		mapName = maps.get(mapId);
		instance.loadMap(mapName);
		mapId++;
	}

	public void randomMap() {
		List<String> maps = instance.getConfig().getStringList("Maps");
		Random rand = new Random();
		int i = rand.nextInt(maps.size());
		mapName = maps.get(i);
		System.out.println("Next map is: " + mapName);
		instance.loadMap(mapName);
		setRandomMap(true);
	}

	public Location getRedTeamSpawn() {
		double x = instance.getConfig().getDouble(mapName + ".TeamRed.Spawn.X");
		double y = instance.getConfig().getDouble(mapName + ".TeamRed.Spawn.Y");
		double z = instance.getConfig().getDouble(mapName + ".TeamRed.Spawn.Z");
		return new Location(Bukkit.getWorld(mapName), x, y, z);
	}

	public Location getBlueTeamSpawn() {
		double x = instance.getConfig().getDouble(mapName + ".TeamBlue.Spawn.X");
		double y = instance.getConfig().getDouble(mapName + ".TeamBlue.Spawn.Y");
		double z = instance.getConfig().getDouble(mapName + ".TeamBlue.Spawn.Z");
		return new Location(Bukkit.getWorld(mapName), x, y, z);
	}

	public boolean isRandomMap() {
		return randomMap;
	}

	public void setRandomMap(boolean randomMap) {
		this.randomMap = randomMap;
	}
}