package net.theminegames.nexus.game;

import java.util.ArrayList;
import java.util.List;

import net.theminegames.nexus.Nexus;
import net.theminegames.nexus.NexusMenu;
import net.theminegames.nexus.apis.ServerApi;
import net.theminegames.nexus.timers.MapTimer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class Game {

	private Scoreboard board;
	private Team teamRed;
	private Team teamBlue;
	private Score redNexus;
	private Score blueNexus;
	private Nexus instance;
	private ArrayList<Player> protection;
	private ArrayList<Location> chestLocations;

	public Game(Nexus instance) {
		this.instance = instance;
		board = Bukkit.getScoreboardManager().getNewScoreboard();
		if (board.getObjective("mmm") == null)
			board.registerNewObjective("mmm", "mmm").setDisplaySlot(DisplaySlot.SIDEBAR);
		board.getObjective("mmm").setDisplayName(ChatColor.GOLD + "Nexus Life");
		redNexus = board.getObjective("mmm").getScore(Bukkit.getOfflinePlayer(ChatColor.RED + "Red Nexus"));
		redNexus.setScore(25);
		blueNexus = board.getObjective("mmm").getScore(Bukkit.getOfflinePlayer(ChatColor.BLUE + "Blue Nexus"));
		blueNexus.setScore(25);

		teamRed = board.registerNewTeam("Red");
		teamBlue = board.registerNewTeam("Blue");

		teamRed.setPrefix(ChatColor.RED + "");
		teamBlue.setPrefix(ChatColor.BLUE + "");

		protection = new ArrayList<Player>();
		chestLocations = new ArrayList<Location>();
	}
	
	public ArrayList<Location> getChestLocations() {
		return chestLocations;
	}
	
	public void addChests(Location loc) {
		chestLocations.add(loc);
	}

	public ArrayList<Player> getSpawnProtection() {
		return protection;
	}

	public void initScoreboard(Player p) {
		p.setScoreboard(board);
	}

	public void initPlayerToTeams(Player p) {
		if (teamRed.getSize() > teamBlue.getSize()) {
			teamBlue.addPlayer(p);
			p.sendMessage(ChatColor.BLUE + "You are on the Blue team.");
		} else {
			teamRed.addPlayer(p);
			p.sendMessage(ChatColor.RED + "You are on the Red team.");
		}
	}

	public void setPlayerListName(Player p) {
		if (teamRed.hasPlayer(p)) {
			String red = ChatColor.RED + p.getName();
			if (red.length() > 16)
				red = red.substring(0, 16);
			p.setPlayerListName(red);
		} else if (teamBlue.hasPlayer(p)) {
			String blue = ChatColor.BLUE + p.getName();
			if (blue.length() > 16)
				blue = blue.substring(0, 16);
			p.setPlayerListName(blue);
		}
	}

	public void onWon() {
		if(instance.getMaps().isRandomMap()) {
			Bukkit.broadcastMessage(ChatColor.GREEN + "[Nexus] " + ChatColor.GOLD + "Game finished. Server restating.");

			for(Player p : Bukkit.getOnlinePlayers()) {
				p.teleport(new Location(Bukkit.getWorld("world"), 0, 200, 0));
				p.getInventory().clear();
			}
			
			instance.unloadMap(instance.getMaps().getMapName());
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
				public void run() {
					for (Player p : Bukkit.getOnlinePlayers()) {
						ServerApi.sendPlayer(p, "hub");
					}
				}
			}, 100);

			Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
				public void run() {
					System.exit(0);
				}
			}, 140L);
			
			return;
		}
		
		if (instance.getMaps().mapRotationFinished()) {
			System.out.println("Finished map rotation");
			Bukkit.broadcastMessage(ChatColor.GREEN + "[Nexus] " + ChatColor.GOLD + "Map rotation finished. Server restarting.");

			for(Player p : Bukkit.getOnlinePlayers()) {
				p.setGameMode(GameMode.ADVENTURE);
				freshStart(p);
			}
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
				public void run() {
					for (Player p : Bukkit.getOnlinePlayers()) {
						ServerApi.sendPlayer(p, "hub");
					}
				}
			}, 100);

			Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
				public void run() {
					System.exit(0);
				}
			}, 140L);
		} else {
			System.out.println("Not finished map rotation");
			
			instance.getMaps().mapRotation();
			
			for (Player p : Bukkit.getOnlinePlayers()) {
				freshStart(p);
				
				instance.newScoreboard(p);
				p.teleport(new Location(Bukkit.getWorld("world"), 0, 200, 0));
				
				instance.getPoints().getPlayerPoints().put(p.getName(), 0);

				if (teamRed.hasPlayer(p)) {
					teamRed.removePlayer(p);
				} else if (teamBlue.hasPlayer(p)) {
					teamBlue.removePlayer(p);
				}
			}
			
			MapTimer.resetTimer();
			MapTimer.start();

			blueNexus.setScore(25);
			redNexus.setScore(25);
		}
	}

	public void freshStart(Player p) {
		p.setHealth(20.0);
		p.setFoodLevel(20);
		p.setSaturation(20.0f);
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		p.setFireTicks(0);
		p.setLevel(0);
		p.setExp(0.0f);
		p.setGameMode(GameMode.SURVIVAL);
		p.setItemOnCursor(null);
		for (PotionEffect pe : p.getActivePotionEffects()) {
			p.removePotionEffect(pe.getType());
		}
	}

	@SuppressWarnings("deprecation")
	public void givePlayerItems(Player p) {
		List<String> items = instance.getConfig().getStringList(instance.getMaps().getMapName() + ".Items");
		for (int i = 0; i < items.size(); i++) {
			String item = items.get(i);
			String[] itemSplit = item.split(",");
			String id = itemSplit[0];
			String amount = itemSplit[1];
			int itemId = Integer.parseInt(id);
			int itemAmount = Integer.parseInt(amount);
			ItemStack theItem = new ItemStack(Material.getMaterial(itemId), itemAmount);
			p.getInventory().addItem(theItem);
			p.getInventory().setItem(8, NexusMenu.getMenuIcon());

			if (teamRed.hasPlayer(p)) {
				ItemStack[] redArmour = { createArmour(Material.LEATHER_BOOTS, Color.RED), createArmour(Material.LEATHER_LEGGINGS, Color.RED), createArmour(Material.LEATHER_CHESTPLATE, Color.RED),
						createArmour(Material.LEATHER_HELMET, Color.RED) };
				p.getInventory().setArmorContents(redArmour);
			} else if (teamBlue.hasPlayer(p)) {
				ItemStack[] blueArmour = { createArmour(Material.LEATHER_BOOTS, Color.BLUE), createArmour(Material.LEATHER_LEGGINGS, Color.BLUE),
						createArmour(Material.LEATHER_CHESTPLATE, Color.BLUE), createArmour(Material.LEATHER_HELMET, Color.BLUE) };
				p.getInventory().setArmorContents(blueArmour);
			}
		}
	}

	private ItemStack createArmour(Material m, Color c) {
		ItemStack is = new ItemStack(m);
		LeatherArmorMeta im = (LeatherArmorMeta) is.getItemMeta();
		im.setColor(c);
		is.setItemMeta(im);
		return is;
	}

	public Team getTeamRed() {
		return teamRed;
	}

	public Team getTeamBlue() {
		return teamBlue;
	}

	public Score getBlueNexus() {
		return blueNexus;
	}

	public Score getRedNexus() {
		return redNexus;
	}
}