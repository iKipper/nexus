package net.theminegames.nexus.game;

import java.util.ArrayList;
import java.util.Random;

import net.theminegames.nexus.Nexus;
import net.theminegames.nexus.utils.ChestItem;

import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Chests {

	public void populateChest(Chest chest) {
		Inventory inventory = chest.getBlockInventory();
		inventory.clear();
		int added = 0;
		Random random = new Random();

		for (ChestItem chestItem : getItems()) {
			if (random.nextInt(100) + 1 <= chestItem.getChance()) {
				inventory.addItem(chestItem.getItem());
				if (added++ > inventory.getSize()) {
					break;
				}
			}
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public static ArrayList<ChestItem> getItems() {
		ArrayList<String> data = (ArrayList<String>) Nexus.getInstance().getConfig().getList(Nexus.getInstance().getMaps().getMapName() + ".ChestItems");
		ArrayList<ChestItem> output = new ArrayList<ChestItem>();

		for (String s : data) {
			String[] values = s.split(", ");
			ChestItem i = new ChestItem(new ItemStack(Integer.parseInt(values[0]), Integer.parseInt(values[1])), Integer.parseInt(values[2]));
			output.add(i);
		}
		return output;
	}
}
