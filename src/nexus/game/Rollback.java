package net.theminegames.nexus.game;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import net.theminegames.nexus.Nexus;

public class Rollback {

	public static void rollback(String map) {
		File srcFolder = new File(Nexus.getInstance().getDataFolder() + File.separator + map);
		File destFolder = new File(map);

		try {
			if (!srcFolder.exists()) {
				System.out.println(Nexus.getInstance().getDatabase() + "");
				System.out.println("World file doesn't exist.");
			} else {
				Nexus.getInstance().unloadMap(map);
				copyFolder(srcFolder, destFolder);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void copyFolder(File src, File dest) throws IOException {
		if (src.isDirectory()) {
			if (!dest.exists()) {
				dest.mkdir();
			}

			String files[] = src.list();

			for (String file : files) {
				File srcFile = new File(src, file);
				File destFile = new File(dest, file);
				copyFolder(srcFile, destFile);
			}

		} else {
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dest);

			byte[] buffer = new byte[1024];

			int length;
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}

			in.close();
			out.close();
		}
	}
}
