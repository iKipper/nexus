package net.theminegames.nexus.utils;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

public class FireworkUtil {
	public static void spawnFirework(Location location, int color) {
		Firework fw = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
		FireworkMeta fwm = fw.getFireworkMeta();
		Type type = Type.BALL_LARGE;
		Color c1 = getColor(color);
		FireworkEffect effect = FireworkEffect.builder().withColor(c1).with(type).build();
		fwm.addEffect(effect);
		fwm.setPower(1);
		fw.setFireworkMeta(fwm);
	}

	private static Color getColor(int i) {
		Color c = null;
		if (i == 1) {
			c = Color.BLUE;
		}
		if (i == 2) {
			c = Color.RED;
		}
		return c;
	}
}
