package net.theminegames.nexus.utils;

import org.bukkit.inventory.ItemStack;

public class ChestItem {

    private ItemStack item;
    private int chance;

    public ChestItem(ItemStack item, int chance) {
        this.item = item;
        this.chance = chance;
    }

    public ItemStack getItem() {
        return item;
    }

    public int getChance() {
        return chance;
    }
}