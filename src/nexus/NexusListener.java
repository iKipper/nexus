package net.theminegames.nexus;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

import me.confuser.barapi.BarAPI;
import net.theminegames.nexus.apis.DatabaseApi;
import net.theminegames.nexus.apis.ServerApi;
import net.theminegames.nexus.game.Chests;
import net.theminegames.nexus.timers.MapTimer;
import net.theminegames.nexus.utils.FireworkUtil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class NexusListener implements Listener {

	private Nexus instance;

	public NexusListener(Nexus instance) {
		this.instance = instance;
	}

	@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent e) {
		e.setJoinMessage(ChatColor.RED + e.getPlayer().getName() + ChatColor.BLUE + " has joined the game. " + ChatColor.GRAY + "(" + Bukkit.getOnlinePlayers().length + "/24)");
		
		BarAPI.setMessage(e.getPlayer(), ChatColor.GOLD + "➠ " + ChatColor.GREEN + "You're playing on " + ChatColor.AQUA + "" + ChatColor.BOLD + "TheMineGames.net");
		
		
		if (DatabaseApi.checkPlayerVIP(e.getPlayer())) {
			System.out.println("Player " + e.getPlayer().getName() + " is a VIP.");
		}

		if (!MapTimer.isFinished()) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
				public void run() {
					for (Player p : Bukkit.getOnlinePlayers()) {
						instance.newScoreboard(p);
					}

					instance.getGame().freshStart(e.getPlayer());
					e.getPlayer().teleport(new Location(Bukkit.getWorld("world"), 0, 200, 0));
					instance.getPoints().getPlayerPoints().put(e.getPlayer().getName(), 0);
				}
			}, 2L);
			if (MapTimer.ready()) {
				if (!MapTimer.isRunning()) {
					MapTimer.start();
				}
			}
		} else {
			instance.getGame().freshStart(e.getPlayer());
			instance.getGame().initScoreboard(e.getPlayer());
			instance.getGame().initPlayerToTeams(e.getPlayer());
			instance.getGame().givePlayerItems(e.getPlayer());
			instance.getPoints().getPlayerPoints().put(e.getPlayer().getName(), 0);

			if (instance.getGame().getTeamBlue().hasPlayer(e.getPlayer())) {
				e.getPlayer().teleport(instance.getMaps().getBlueTeamSpawn());
			} else {
				e.getPlayer().teleport(instance.getMaps().getRedTeamSpawn());
			}
		}
	}

	@EventHandler
	public void onPlayerQuit(final PlayerQuitEvent e) {
		e.setQuitMessage(null);
		Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
			public void run() {
				Bukkit.broadcastMessage(ChatColor.RED + e.getPlayer().getName() + ChatColor.BLUE + " has quit the game. " + ChatColor.GRAY + "(" + Bukkit.getOnlinePlayers().length + "/24)");
				instance.getPoints().getPlayerPoints().remove(e.getPlayer().getName());

				if (instance.getGame().getTeamRed().hasPlayer(e.getPlayer())) {
					instance.getGame().getTeamRed().removePlayer(e.getPlayer());
				} else if (instance.getGame().getTeamBlue().hasPlayer(e.getPlayer())) {
					instance.getGame().getTeamBlue().removePlayer(e.getPlayer());
				}

				if (MapTimer.isFinished()) {
					if (Bukkit.getOnlinePlayers().length < 2) {
						for (Player p : Bukkit.getOnlinePlayers()) {
							ServerApi.sendPlayer(p, "hub");
						}

						Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
							public void run() {
								System.exit(0);
							}
						}, 20L);
					}
				}
			}
		}, 2L);
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {
		final Block b = e.getBlock();
		if (b.getRelative(BlockFace.DOWN, 1).getType().equals(Material.BEDROCK)) {
			e.getPlayer().sendMessage(ChatColor.RED + "You can not build in this area.");
			e.setCancelled(true);
		}
		if (b.getRelative(BlockFace.DOWN, 2).getType().equals(Material.BEDROCK)) {
			e.getPlayer().sendMessage(ChatColor.RED + "You can not build in this area.");
			e.setCancelled(true);
		}
		if (b.getRelative(BlockFace.DOWN, 3).getType().equals(Material.BEDROCK)) {
			e.getPlayer().sendMessage(ChatColor.RED + "You can not build in this area.");
			e.setCancelled(true);
		}
		if (b.getRelative(BlockFace.DOWN, 4).getType().equals(Material.BEDROCK)) {
			e.getPlayer().sendMessage(ChatColor.RED + "You can not build in this area.");
			e.setCancelled(true);
		}

		if (!MapTimer.isFinished()) {
			e.setCancelled(true);
		}

		if (b.getType() == Material.TNT) {
			b.setType(Material.AIR);
			Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
				public void run() {
					b.getWorld().createExplosion(b.getLocation(), 3, false);
				}
			}, 40L);
		}
		
		if(b.getType() == Material.CHEST) {
			instance.getGame().addChests(b.getLocation());
		}
	}
	
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent e) {
		List<Block> destroyed = e.blockList();
        Iterator<Block> it = destroyed.iterator();
        while (it.hasNext()) {
            Block block = it.next();
            if (block.getType() == Material.QUARTZ_ORE)
                it.remove();
        }
	}

	@EventHandler
	public void onBlockBreak(final BlockBreakEvent e) {
		if (e.getBlock().getLocation().getX() == instance.getConfig().getDouble(instance.getMaps().getMapName() + ".TeamRed.Nexus.X")
				&& e.getBlock().getLocation().getY() == instance.getConfig().getDouble(instance.getMaps().getMapName() + ".TeamRed.Nexus.Y")
				&& e.getBlock().getLocation().getZ() == instance.getConfig().getDouble(instance.getMaps().getMapName() + ".TeamRed.Nexus.Z")) {
			if (instance.getGame().getTeamRed().hasPlayer(e.getPlayer())) {
				e.getPlayer().sendMessage(ChatColor.RED + "This is your nexus. Defend it.");
				e.setCancelled(true);
			} else {
				instance.getGame().getRedNexus().setScore(instance.getGame().getRedNexus().getScore() - 1);

				FireworkUtil.spawnFirework(e.getBlock().getLocation(), 2);
				Bukkit.getWorld(instance.getMaps().getMapName()).playEffect(e.getBlock().getLocation(), Effect.MOBSPAWNER_FLAMES, 40);

				Bukkit.broadcastMessage(ChatColor.GREEN + "[Nexus] " + ChatColor.BLUE + e.getPlayer().getName() + ChatColor.GOLD + " is damaging the nexus.");
				e.setCancelled(true);
				if (instance.getGame().getRedNexus().getScore() == 0) {
					Bukkit.broadcastMessage(ChatColor.BLUE + "        " + ChatColor.MAGIC + "*** " + ChatColor.RESET + "" + ChatColor.BLUE + "Blue team has won" + ChatColor.MAGIC + " ***");
					for (Player p : Bukkit.getOnlinePlayers()) {
						if (instance.getGame().getTeamBlue().hasPlayer(p)) {
							DatabaseApi.addPlayerTokens(p, 2);
						} else if (instance.getGame().getTeamRed().hasPlayer(p)) {
							DatabaseApi.addPlayerTokens(p, 1);
						}
					}
					instance.getGame().onWon();
				}
			}
		} else if (e.getBlock().getLocation().getX() == instance.getConfig().getDouble(instance.getMaps().getMapName() + ".TeamBlue.Nexus.X")
				&& e.getBlock().getLocation().getY() == instance.getConfig().getDouble(instance.getMaps().getMapName() + ".TeamBlue.Nexus.Y")
				&& e.getBlock().getLocation().getZ() == instance.getConfig().getDouble(instance.getMaps().getMapName() + ".TeamBlue.Nexus.Z")) {
			if (instance.getGame().getTeamBlue().hasPlayer(e.getPlayer())) {
				e.getPlayer().sendMessage(ChatColor.BLUE + "This is your nexus. Defend it.");
				e.setCancelled(true);
			} else {
				instance.getGame().getBlueNexus().setScore(instance.getGame().getBlueNexus().getScore() - 1);

				FireworkUtil.spawnFirework(e.getBlock().getLocation(), 1);
				Bukkit.getWorld(instance.getMaps().getMapName()).playEffect(e.getBlock().getLocation(), Effect.MOBSPAWNER_FLAMES, 40);

				Bukkit.broadcastMessage(ChatColor.GREEN + "[Nexus] " + ChatColor.RED + e.getPlayer().getName() + ChatColor.GOLD + " is damaging the nexus.");
				e.setCancelled(true);
				if (instance.getGame().getBlueNexus().getScore() == 0) {
					Bukkit.broadcastMessage(ChatColor.RED + "        " + ChatColor.MAGIC + "*** " + ChatColor.RESET + "" + ChatColor.RED + "Red team has won" + ChatColor.MAGIC + " ***");
					for (Player p : Bukkit.getOnlinePlayers()) {
						if (instance.getGame().getTeamBlue().hasPlayer(p)) {
							DatabaseApi.addPlayerTokens(p, 1);
						} else if (instance.getGame().getTeamRed().hasPlayer(p)) {
							DatabaseApi.addPlayerTokens(p, 2);
						}
					}
					instance.getGame().onWon();
				}
			}
		}

		if (!MapTimer.isFinished()) {
			e.setCancelled(true);
		}
		
		
		if(!e.getBlock().getLocation().getWorld().getName().equalsIgnoreCase("world")) {
			if (e.getBlock().getType() == Material.STONE) {
				final Random rand = new Random();
				int i = rand.nextInt(100) + 1;
				if (i < 4) {
					e.getPlayer().sendMessage(ChatColor.RED + "You found a random chest!");
					Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
						public void run() {
							e.getBlock().getLocation().getWorld().getBlockAt(e.getBlock().getLocation()).setType(Material.CHEST);
						}
					}, 5);
				}
			}	
		}
		
		if(e.getBlock().getWorld().getName().equalsIgnoreCase("world")) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		if (MapTimer.isRunning() || MapTimer.isRunning()) {
			e.setCancelled(true);
		}
		
		if (e.getDamager().getType() == EntityType.ARROW) {
			Arrow arrow = (Arrow) e.getDamager();
			Player p = (Player) e.getEntity();
			Player s = (Player) arrow.getShooter();
			if (instance.getGame().getTeamRed().hasPlayer(p) && instance.getGame().getTeamRed().hasPlayer(s)) {
				e.setCancelled(true);
			}

			if (instance.getGame().getTeamBlue().hasPlayer(p) && instance.getGame().getTeamBlue().hasPlayer(s)) {
				e.setCancelled(true);
			}
		} else {
			Player p = (Player) e.getEntity();
			Player k = (Player) e.getDamager();

			if (instance.getGame().getTeamRed().hasPlayer(p) && instance.getGame().getTeamRed().hasPlayer(k)) {
				e.setCancelled(true);
			}

			if (instance.getGame().getTeamBlue().hasPlayer(p) && instance.getGame().getTeamBlue().hasPlayer(k)) {
				e.setCancelled(true);
			}

			if (instance.getGame().getSpawnProtection().contains(p)) {
				k.sendMessage(ChatColor.RED + "That player has spawn protection.");
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onEntityDamage(EntityDamageEvent e) {
		if (instance.getGame().getSpawnProtection().contains(e.getEntity())) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if (e.getPlayer().getItemInHand().getType() == Material.BUCKET) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		final Player p = e.getEntity();
		Player k = p.getKiller();
		p.getWorld().playEffect(p.getLocation(), Effect.SMOKE, 30);

		if (k != null) {
			if (instance.getGame().getTeamBlue().hasPlayer(p)) {
				e.setDeathMessage(ChatColor.BLUE + p.getName() + ChatColor.GOLD + " was killed by " + ChatColor.RED + k.getName());
			} else if (instance.getGame().getTeamRed().hasPlayer(p)) {
				e.setDeathMessage(ChatColor.RED + p.getName() + ChatColor.GOLD + " was killed by " + ChatColor.BLUE + k.getName());
			}
		} else {
			e.setDeathMessage(null);
		}

		instance.getGame().freshStart(p);
		Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
			public void run() {
				instance.getGame().getSpawnProtection().add(p);
				instance.getGame().givePlayerItems(p);
				if (instance.getGame().getTeamBlue().hasPlayer(p)) {
					p.teleport(instance.getMaps().getBlueTeamSpawn());
				} else if (instance.getGame().getTeamRed().hasPlayer(p)) {
					p.teleport(instance.getMaps().getRedTeamSpawn());
				}
			}
		}, 5);
		Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
			public void run() {
				instance.getGame().getSpawnProtection().remove(p);
			}
		}, 60);

		e.setDroppedExp(0);
		e.getDrops().clear();

		if (k != null) {
			if (DatabaseApi.checkPlayerVIP(k) || DatabaseApi.getPlayerRank(k).equalsIgnoreCase("Admin")) {
				instance.getPoints().getPlayerPoints().put(k.getName(), instance.getPoints().getPlayerPoints().get(k.getName()) + 3);
			} else {
				instance.getPoints().getPlayerPoints().put(k.getName(), instance.getPoints().getPlayerPoints().get(k.getName()) + 1);
			}
		}
	}

	@EventHandler
	public void onServerPing(ServerListPingEvent e) {
		if (!MapTimer.isFinished() && !MapTimer.isRunning()) {
			e.setMotd("Waiting");
		}
		if (MapTimer.isFinished()) {
			e.setMotd(instance.getMaps().getMapName());
		} else {
			e.setMotd("Starts in " + Nexus.millisToReadable(MapTimer.getTimer()));
		}
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e) {
		if (!MapTimer.isFinished()) {
			if (DatabaseApi.getPlayerRank(e.getPlayer()).equalsIgnoreCase("Admin")) {
				e.setFormat(ChatColor.RED + "[Admin] " + e.getPlayer().getName() + ": " + e.getMessage());
			} else if (DatabaseApi.getPlayerRank(e.getPlayer()).equalsIgnoreCase("Builder")) {
				e.setFormat(ChatColor.BLUE + "[Builder] " + e.getPlayer().getName() + ": " + e.getMessage());
			} else if (DatabaseApi.getPlayerRank(e.getPlayer()).equalsIgnoreCase("VIP ")) {
				e.setFormat(ChatColor.YELLOW + "[VIP] " + e.getPlayer().getName() + ": " + ChatColor.WHITE + e.getMessage());
			} else {
				e.setFormat(ChatColor.GRAY + e.getPlayer().getName() + ": " + e.getMessage());
			}
		} else {
			if (instance.getGame().getTeamBlue().hasPlayer(e.getPlayer())) {
				e.setFormat(ChatColor.BLUE + "[Blue] " + e.getPlayer().getName() + ChatColor.GRAY + ": " + e.getMessage());
			} else if (instance.getGame().getTeamRed().hasPlayer(e.getPlayer())) {
				e.setFormat(ChatColor.RED + "[Red] " + e.getPlayer().getName() + ChatColor.GRAY + ": " + e.getMessage());
			}
		}
	}

	@EventHandler
	public void onInventoryOpen(InventoryOpenEvent e) {
		if (e.getInventory().getHolder() instanceof Chest) {
			Chest chest = (Chest) e.getInventory().getHolder();
			Location loc = chest.getLocation();

			if (!instance.getGame().getChestLocations().contains(loc)) {
				instance.getGame().addChests(loc);
				Chests chests = new Chests();
				chests.populateChest(chest);
			}
		}
	}
	
	@EventHandler
	public void onWeatherChange(WeatherChangeEvent e) {
		e.setCancelled(true);
	}
}